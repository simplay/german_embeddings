import os
import glob


def write_to_output_file(filename: str, songs: []) -> []:
    with open(filename, 'a') as file:
        content = "\n".join(songs)
        file.write(content)
    return []


def extract_songs_content(directory_name, songs):
    filepath = os.path.join(base_path, directory_name)
    song_file_pattern = f"{filepath}/*.txt"
    for filepath in glob.glob(song_file_pattern):
        with open(filepath) as file:
            lines = file.read()
            lines = lines.replace("?", ".")
            lines = lines.replace("!", ".")
            lines = lines.replace("…", "")
            lines = lines.replace(",", "")
            lines = lines.replace("’", "")
            lines = lines.replace("ß", "ss")
            lines = lines.replace("\n", "")
            lines = lines.replace("-", " ")
            lines = lines.replace("…", " ")
            lines = lines.replace(",", " ")
            lines = lines.replace("’", "")
            lines = lines.replace("\"", "")
            lines = lines.replace("„", "")

            lines = lines.strip()
            songs.append(lines)

    return songs


if __name__ == "__main__":
    base_path = "/mnt/Data/magi/textkorpus/songs_postprocessed"

    songs = []
    songs = extract_songs_content("out_song_li", songs)
    songs = extract_songs_content("out_song_we", songs)
    write_to_output_file("out/songs.clean.corpus", songs)

    song_text_sentences = []
    for song_text in songs:
        song_text.replace("!", ".")
        song_text.replace("?", ".")

        for line in song_text.split("."):
            line = line.strip()
            if len(line) > 0:
                song_text_sentences.append(f"{line}.")

    write_to_output_file("out/songs.clean.corpus.clustering", song_text_sentences)

    print("Removing tags")
    cmd = "sed -i 's/<[^>]*>//g' out/songs.clean.corpus"
    print(os.popen(cmd).read())

    cmd = "sed -i 's/<[^>]*>//g' out/songs.clean.corpus.clustering"
    print(os.popen(cmd).read())
