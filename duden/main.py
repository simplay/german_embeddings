import sys

import all_ddefs as duden
from pymongo import MongoClient

import logging
import os
#from dotenv import load_dotenv

#load_dotenv()
import time


def run(batchsize: int):
    #### create the different MongoDB-Databases
    mongo_user = os.getenv("MONGODB_USER")
    mongo_pw = os.getenv("MONGODB_PW")
    mongo_host = os.getenv("MONGODB_HOST")
    myclient = MongoClient(mongo_host, port=27017)
    DudenDB = myclient["DudenDB"]

    # Table for the URLS
    URLs = DudenDB["URLs"]
    # Table for the extracted content
    Main = DudenDB["Main"]

    start_URL = 'https://www.duden.de/synonyme/Pflanze'

    if ('URLs' in DudenDB.list_collection_names()) != True:
        URLs.insert_one({'url': start_URL, 'done': 0})

    logging.basicConfig(level=logging.INFO)

    joblist = [1]
    while len(joblist) > 0:
        logging.info("Start of loop")
        try:
            allEntrys = URLs.find()
            allEntrys_list = list(allEntrys)

            All_URLs = set([i.get('url', None) for i in allEntrys_list])

            undone_URL = [i.get('url', None) for i in allEntrys_list if i['done'] == 0]
            undone_IDs = [i.get('_id', None) for i in allEntrys_list if i['done'] == 0]

            # batchSize = 100

            joblist = undone_URL[:batchsize]
            ids = undone_IDs[:batchsize]

            output = list(map(duden.ProcessURL, joblist))
            newURLS_temp, newContent = list(list(zip(*output)))

            NewURL_Set = set([item for sublist in newURLS_temp for item in sublist])

            RealNew = NewURL_Set - All_URLs

            try:
                Main.insert_many(newContent)
                if len(RealNew) > 0:
                    URLs.insert_many(duden.convertSetToDict(RealNew))
                URLs.update_many({"_id": {"$in": ids}}, {"$set": {"done": 1}})
            except:
                logging.error(RealNew)
                logging.error('Error in URLSet')
                continue

        except KeyboardInterrupt:
            logging.info('\n Program interrupted by user...')
            break


if __name__ == '__main__':
    batchsize = int(sys.argv[1]) or 10
    run(batchsize=batchsize)
