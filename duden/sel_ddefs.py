from bs4 import BeautifulSoup
import re
from datetime import datetime
import requests
import logging

# url = 'https://www.duden.de/rechtschreibung/Brot'

def get_soup_from_url(url):
    logging.info(url)
    try:
        document = requests.get(url)
        html = document.content

        if document.status_code != 200:
            logging.error(url)
            logging.error("Error on page: ", document.status_code)

        if 'text/html' not in document.headers['content-type']:
            logging.error(url)
            logging.error("Ignore non text/html page")
    except:
        logging.error("error in URL:" + url)
        return None

    return BeautifulSoup(html, "html.parser")

#html_soup_raw = get_soup_from_url(url)


def reduce_HTML_To_Essential(html_soup):
    '''
    removes elements from the HTML, to reduce the numbers of strings that have to be saved in the DB
    @param html_soup:
    @return:
    '''
    # All relevant Text is part of the article Tag
    article = html_soup.find_all('article', {"role": "article"})[0]

    # Elements after aside are elements from the classes class_="lilamishakanakami" class="felinanakanakami"
    # class="falanaliminakami", they probabably contain elements for styling...
    # Elements after script contain JavaScript code for Interaction
    # Elements after Figure contain figure descriptions or links from figures

    unwanted = article.find_all(['aside', 'script', 'figure'])
    for item in unwanted:
        item.extract()

    # Elements after more__links contain duplicates of ID Synonyms
    # Elements after info-ref contain InfoHeaders
    # Elements after division header contain Titles
    # Elements after gizmo_gadgets are for interaction
    # Elements after wrap-table: contain the Grammatik-Tabelle in the Grammatik part
    unwanted = article.findAll(True,
                               {'class': ['more__link', 'info-ref', 'gizmo__gadget', 'wrap-table', 'division__header']})
    for item in unwanted:
        item.extract()

    return article

def modify_Text_for_Annotations(article):
    # modify text to mark as annotation
    annotation = article.find_all('dt', class_=["tuple__key", "note__title"])
    if len(annotation) > 0:
        for item in annotation:
            if item is not None:
                item.string = "{" + item.string + "}"  # Text in {...} is  notational, its important to get the context...

    return article

def ProcessURL(url):
    base_url = 'https://www.duden.de'
    html = get_soup_from_url(url)
    new_urls = []

    if html is not None:
        hookups = html.findAll("div", {"class": "hookup"})
        for hookup in hookups:
            for anker_tag in hookup("a"):
                new_urls.append(base_url + anker_tag.get('href'))
    content = extractContent(url, html)

    return (new_urls, content)

def writeToDict(set_entry: str) -> dict:
    '''
    Function to rewrite the string entry into a dictionary that can later be read into the MongoDB
    @param set_entry: string entry with the URL to convert
    @return: dict with the entry and the done set as 0 (int)
    '''
    return {'url': set_entry, 'done': 0}


def convertSetToDict(url_set):
    """
    Converts the set of urls into a dictionary that can be read into the MongoDB with insert_many
    @param url_set: Set of new urls that are converted into a list of dicts
    @return: list of dictionaries with the new URLs
    """
    return list(map(writeToDict, list(url_set)))


def get_Bedeutung(article):
    definition_text = ""
    definitions = article.findAll('div', class_=["xerox"])
    if len(definitions) > 0:
        for definition in definitions:
            defin1 = definition.findAll('ul', class_="xerox__group")
            definition_text = definition_text + defin1[0].get_text().strip() + "\n"
    return definition_text

def extractContent(url, html_soup_raw):
    if html_soup_raw is None:
        bb = None
        word = None
        html_soup = "-1"
    else:
        html_soup = reduce_HTML_To_Essential(html_soup_raw)
        html_soup = modify_Text_for_Annotations(html_soup)

        bb = get_Bedeutung(html_soup)
        w1 = html_soup.findAll('h1', class_=["rubric"])
        w2 = w1[0].find_all('em')
        word = w2[0].get_text()

    return {'url': url,
            'word': word,
            'Bedeutung': bb,
            'html': str(html_soup)}
