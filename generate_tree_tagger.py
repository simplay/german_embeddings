import os

if __name__ == "__main__":
    base_name = "knorke"
    base_name = "songs"
    base_name = "dewiki_01062021"
    base_name = "news.2011"
    #base_name = "news_2012"
    #base_name = "movie_subs"

    input_filepath = f"out/{base_name}.clean3.corpus"
    output_filepath = f"out/{base_name}.corpus.treetagger"

    cmd = f"tree_tagger/cmd/tree-tagger-german {input_filepath} > {output_filepath}"
    os.popen(cmd).read()
