import os
import glob


def write_to_output_file(filename: str, articles: []) -> []:
    with open(filename, 'w') as file:
        file.write(articles[0])
    return []


def extract_news_content(filename, articles):
    base_path = "/home/simplay/repos/embeddings/news_preprocessing"
    filepath = os.path.join(base_path, filename)
    song_file_pattern = f"{filepath}"
    for filepath in glob.glob(song_file_pattern):
        with open(filepath) as file:
            lines = file.read()

            lines = lines.replace("?", ".")
            lines = lines.replace("!", ".")
            lines = lines.replace("…", "")
            lines = lines.replace(",", "")
            lines = lines.replace("’", "")
            lines = lines.replace("ß", "ss")
            lines = lines.replace("-", " ")
            lines = lines.replace("…", " ")
            lines = lines.replace(",", " ")
            lines = lines.replace("’", "")
            lines = lines.replace("\"", "")
            lines = lines.replace("„", "")
            articles.append(lines)

    return articles


if __name__ == "__main__":
    articles = []
    filename = "newstxt2012.txt"

    articles = extract_news_content(filename, articles)
    write_to_output_file(f"/home/simplay/repos/embeddings/out/news_{filename}.clean.corpus", articles)

    print("Removing tags")
    cmd = f"sed -i 's/<[^>]*>//g' /home/simplay/repos/embeddings/out/news_{filename}.clean.corpus"
    print(os.popen(cmd).read())
