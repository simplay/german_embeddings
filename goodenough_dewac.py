import json
import gensim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import openpyxl
import pickle
import seaborn as sns

#scritp produced 11.august to test freshly trained cbow model on whole corpus (including subtitles) and inverse mapping

cat_vecs = os.path.dirname('/mnt/Data/german_embeddings/guenther/')
#model_analysis = os.path.dirname('C:/Users/Magdalena/Documents/PhD/Projekt2/analysis/')

os.chdir(cat_vecs)

with open(r"vectors_dewac_cbow.txt", "r", encoding='cp1252') as file:
    cont_p = file.readlines()

dic = {}
for i in cont_p:
    lst = i.split('"')
    lst2 = lst[2].split(' ')
    num = lst2[1:]
    num_lst = num[-1].split('\n')
    num[-1] = num_lst[0]
    dic[lst[1]] = num

lst_ans = pd.read_excel('subtitles2_300_humrats_11aug21.xlsx')
lst_wrds = list(lst_ans['Wort1'])
lst_trgts = list(lst_ans['Wort2'])
id = list(lst_ans['ID'])

lst_new = []
for i in lst_wrds:
    i = i.rstrip()
    rep = i.replace('ä','ae')
    rep = rep.replace('ö','oe')
    rep = rep.replace('ü','ue')
    rep = rep.replace('Ü', 'Ue')
    rep = rep.replace('Ä', 'Ae')
    rep = rep.replace('Ö', 'Oe')
    rep = rep.replace('ß','ss')
    lst_new.append(rep)

lst_new_trs = []
for i in lst_trgts:
    i = i.rstrip()
    rep = i.replace('ä','ae')
    rep = rep.replace('ö','oe')
    rep = rep.replace('ü','ue')
    rep = rep.replace('Ü', 'Ue')
    rep = rep.replace('Ä', 'Ae')
    rep = rep.replace('Ö', 'Oe')
    rep = rep.replace('ß','ss')
    lst_new_trs.append(rep)


from numpy import dot
from numpy.linalg import norm

def cosine_similarity(list_1, list_2):
    cos_sim = dot(np.array(list_1,dtype=float), np.array(list_2,dtype=float)) / (norm(list_1) * norm(list_2))
    return cos_sim

lst_freq = []
lst_fail = []
for idx, i in enumerate(lst_new):
    try:
        v1 = dic[lst_new_trs[idx].lower()]
        v2 = dic[i.lower()]
        lst_freq.append(cosine_similarity(v1, v2))
    except:
        lst_fail.append(idx)
        lst_freq.append('n.a.')

d = {'ID':id,'Wort1': lst_new_trs, 'Wort2': lst_new, 'cosine': lst_freq}
df = pd.DataFrame(d)

data2= lst_ans.set_index("ID").join(df.set_index("ID")['cosine'])

data2.to_excel('300_humrat_dewac.xlsx',index=True)