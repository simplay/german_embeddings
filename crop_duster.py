import os
from nltk.corpus import stopwords


def normalize_word(word: str):
    word = word.replace('ä', 'ae')
    word = word.replace('ö', 'oe')
    word = word.replace('ü', 'ue')
    word = word.replace('Ä', 'Ae')
    word = word.replace('Ö', 'Oe')
    word = word.replace('Ü', 'Ue')
    word = word.replace('ß', 'ss')
    return word


def main():
    misfits = stopwords.words('german')
    misfits = map(lambda word: normalize_word(word), misfits)

    # filter misfits from dataset


if __name__ == "main":
    main()
