
import os
import glob


def write_to_output_file(filename: str, songs: []) -> []:
    with open(filename, 'a') as file:
        content = "\n".join(songs)
        file.write(content)
    return []

def extract_songs_content(directory_name):
    filepath = os.path.join(base_path, directory_name)
    with open(filepath) as file:
        lines = file.read()
        lines = lines.replace("?", ".")
        lines = lines.replace("!", ".")
        lines = lines.replace("…", "")
        lines = lines.replace(",", "")
        lines = lines.replace("’", "")
        lines = lines.replace("ß", "ss")
        lines = lines.replace("\n", "")
        lines = lines.replace("-", " ")
        lines = lines.replace("…", " ")
        lines = lines.replace(",", " ")
        lines = lines.replace("’", "")
        lines = lines.replace("\"", "")
        lines = lines.replace("„", "")

        lines = lines.strip()
        songs.append(lines)

    return songs

if __name__ == "__main__":
    base_path = "/home/simplay/Documents/phd/textkorpus/songs_postprocessed"

    songs = []
    songs = extract_songs_content("out_song_li", songs)
    songs = extract_songs_content("out_song_we", songs)
    write_to_output_file("out/songs.clean.corpus", songs)

    print("Removing tags")
    cmd = "sed -i 's/<[^>]*>//g' out/songs.clean.corpus"
    print(os.popen(cmd).read())
