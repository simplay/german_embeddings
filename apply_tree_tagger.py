import sys
import os
import time
import subprocess

UPDATE_ITERATION_COUNT = 250


def process_line(line: str) -> [str, bool]:
    splits = line.strip().split("\t")
    if not len(splits) == 3:
        return None, False

    raw_word, text_type, normalized_word = splits

    if raw_word == "@card@" or normalized_word == "@card@":
        return None, False

    if len(normalized_word) < 2 and text_type[0] != "$":
        return None, False

    if text_type[0] == "$":
        if raw_word == "." or raw_word == "?":
            return f"{raw_word}\n", True
        else:
            return None, False


    elif text_type == "CARD":
        # TODO: use raw_word here
        return None, False
    elif text_type == "ADJA":
        # TODO: use raw_word here
        return normalized_word, True
    elif text_type == "ADV":
        # TODO: use raw_word here
        return normalized_word, True

    elif normalized_word == "@card@":
        # TODO: use raw_word here
        print(text_type)
        return None, False


    # e.g.
    # "am" -> "an+die
    # "im" -> "in+die
    # "zum" -> "zu+di
    elif text_type == "APPRART":
        first_word, *_ = normalized_word.split("+")
        return first_word, True

    elif text_type == "NN":
        if normalized_word == "<unknown>":
            return raw_word, True

        return normalized_word, True

    # foreign language words
    elif text_type == "FM":
        # TODO: can we remove this?
        return None, False

    # locations, places
    elif text_type == "NE":
        if normalized_word == "<unknown>":
            return raw_word, True

        return normalized_word, True

    elif text_type == "WFIN":
        return normalized_word, True

    if normalized_word == "<unknown>":
        return None, False
        # return raw_word, True

    normalized_word = normalized_word.replace("ß", "ss")

    if normalized_word == "<unknown>":
        return raw_word, True

    return normalized_word, True


def write_to_output_file(filename: str, words: []) -> []:
    with open(filename, 'a') as file:
        content = " ".join(words)
        file.write(content)
    return []


if __name__ == "__main__":
    # apply tree_tagger sentence after sentence
    # _, filepath, *_ = sys.argv

    # input_filepath = "knorke/knorke.corpus"
    base_name = "knorke"
    base_name = "songs"
    base_name = "dewiki_01062021"
    base_name = "news.2011"
    #base_name = "news_2012"
    #base_name = "movie_subs"

    input_filepath = f"out/{base_name}.corpus.treetagger"
    output_filepath = f"out/{base_name}.mapped.corpus"

    sentences = []

    cmd = f"sed -n '$=' {input_filepath}"
    line_count = int(os.popen(cmd).read())
    words = []

    with open(input_filepath) as file:
        for line_index, line in enumerate(file, start=1):

            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")

            # if line_index > 10:
            #     break
            word, should_append_word_to_list = process_line(line)
            if should_append_word_to_list:
                words.append(word.split("|")[0])

    write_to_output_file(filename=output_filepath, words=words)
