import os
import json
import hashlib
import csv
from collections import Counter

UPDATE_ITERATION_COUNT = 250


def generate_inverse_mapping(synonym_groups):
    normalized_synonym_json_data = {}
    for synonyms in synonym_groups:
        sentinel = "".join(synonyms).encode("utf8")
        #sentinel = hashlib.md5(sentinel).hexdigest()
        sentinel = synonyms[0]

        for word in synonyms:
            normalized_synonym_json_data[word] = sentinel
    return normalized_synonym_json_data


def process_line(line: str) -> [str, bool]:
    splits = line.strip().split("\t")
    if not len(splits) == 3:
        return None, False

    raw_word, text_type, normalized_word = splits

    if raw_word == "@card@" or normalized_word == "@card@":
        return raw_word, True

    if len(normalized_word) < 2 and text_type[0] != "$":
        return raw_word, True

    if text_type[0] == "$":
        if raw_word == "!":
            return "!", False

        if raw_word == "." or raw_word == "?":
            return f"{raw_word}\n", True
        else:
            return raw_word, True


    elif text_type == "CARD":
        return raw_word, True

    elif text_type == "ADJA":
        # TODO: use raw_word here
        if normalized_word.__contains__("unknown"):
            return raw_word, True

        return normalized_word, True
    elif text_type == "ADV":
        if normalized_word.__contains__("unknown"):
            return raw_word, True

        # TODO: use raw_word here
        return normalized_word, True

    elif normalized_word == "@card@":
        # TODO: use raw_word here
        print(text_type)
        return raw_word, True


    # e.g.
    # "am" -> "an+die
    # "im" -> "in+die
    # "zum" -> "zu+di
    elif text_type == "APPRART":
        first_word, *_ = normalized_word.split("+")
        return first_word, True

    elif text_type == "NN":
        if normalized_word == "<unknown>":
            return raw_word, True

        # prevent false positives: flöten (used as an adjective) to "Flöte"
        if normalized_word[0] != raw_word[0]:
            return raw_word, True

        return normalized_word, True

    # foreign language words
    elif text_type == "FM":
        # TODO: can we remove this?
        return raw_word, True

    # locations, places
    elif text_type == "NE":
        return raw_word, True
    elif text_type == "WFIN":
        return normalized_word, True

    if normalized_word == "<unknown>":
        return raw_word, True

    normalized_word = normalized_word.replace("ß", "ss")

    if normalized_word == "<unknown>":
        return raw_word, True

    return normalized_word, True


def write_to_output_file(filename: str, words: []) -> []:
    with open(filename, 'a') as file:
        content = " ".join(words)
        file.write(content)
    return []


def normalize_word(word):
    w = word.replace("!", "")
    w = w.replace("?", "")
    w = w.replace(".", "")

    return w


def apply_treetagger(input_filepath: str) -> list:
    cmd = f"sed -n '$=' {input_filepath}"
    line_count = int(os.popen(cmd).read())
    words = []

    lines = []
    with open(input_filepath) as file:
        for line_index, line in enumerate(file, start=1):

            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")

            word, should_append_word_to_list = process_line(line)

            if word.__contains__("unknown"):
                print(f"line {line} was mapped to unknown")

            if word == "!":
                if len(words) == 0:
                    print("empty!!")

                else:
                    lines.append(words)
                words = []

            if should_append_word_to_list:
                word = word.split("|")[0]
                word = word.strip().replace("ä", "ae")
                word = word.replace("ß", "ss")
                word = word.replace("ö", "oe")
                word = word.replace("ü", "ue")
                word = word.replace("Ä", "Ae")
                word = word.replace("Ö", "Oe")
                word = word.replace("Ü", "Ue")

                words.append(word)

    return lines


def apply_treetagger_for_homonyms(input_filepath: str) -> list:
    cmd = f"sed -n '$=' {input_filepath}"
    line_count = int(os.popen(cmd).read())
    words = []

    lines = []
    with open(input_filepath) as file:
        for line_index, line in enumerate(file, start=1):

            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")

            word, should_append_word_to_list = process_line(line)

            if word.__contains__("unknown"):
                print(f"line {line} was mapped to unknown")

            if should_append_word_to_list:
                word = word.split("|")[0]
                word = word.strip().replace("ä", "ae")
                word = word.replace("ß", "ss")
                word = word.replace("ö", "oe")
                word = word.replace("ü", "ue")
                word = word.replace("Ä", "Ae")
                word = word.replace("Ö", "Oe")
                word = word.replace("Ü", "Ue")

                lines.append(word)

    return lines


def load_synonym_lines(input_filepath: str, output_filepath: str) -> list:
    lines = []
    with open(input_filepath) as file:
        synonyms = json.load(file)

        for key, values in synonyms.items():
            if len(values) == 0:
                print("empty")

            key = normalize_word(key)
            values = [normalize_word(v) for v in values]

            lines.append(" ".join([key, *values, "!"]))

    synonym_lines_filepath = f"synonym_lines.txt"
    with open(synonym_lines_filepath, 'w') as file:
        file.write("\n".join(lines))

    cmd = f"../tree_tagger/cmd/tree-tagger-german {synonym_lines_filepath} > {output_filepath}"
    os.popen(cmd).read()
    os.remove(synonym_lines_filepath)


def filter_synonyms_by_homonyms(synonyms: dict, homonyms: list) -> dict:
    # TODO: apply filtering here
    removal_counter = 0
    for homonym in homonyms:
        if synonyms.__contains__(homonym):
            removal_counter += 1
            synonyms.pop(homonym)

    print(f"Removed {removal_counter} synonyms")

    return synonyms


def load_homonym_lines(input_filepath: str, output_filepath: str) -> list:
    with open(input_filepath) as file:
        reader = csv.reader(file, delimiter=';')

        items = []
        for row_index, row in enumerate(reader):
            if row_index == 0:
                continue

            right_side = " ".join(row[1:]).strip()

            # INFO: this depends highly on the used dataset, currently, the quality of the dataset is rather poor.
            if len(right_side.split(" ")) == 1:
                items.append(row[0])
                items.append(right_side)
            else:
                print(row)
    homonym_lines_filepath = f"homonym_lines.txt"
    with open(homonym_lines_filepath, 'w') as file:
        file.write("\n".join(items))

    cmd = f"../tree_tagger/cmd/tree-tagger-german {homonym_lines_filepath} > {output_filepath}"
    os.popen(cmd).read()
    os.remove(homonym_lines_filepath)

    return items


def main():
    treetagger_syn_output_filename = "treetaggerd_syn.txt"
    treetagger_homo_output_filename = "treetaggerd_hom.txt"

    homonym_filepath = "../data/homonym_final_11aug21.csv"
    # synonym_filepath = "../data/syn_dict.json"
    synonym_filepath = "../data/new_synonyms_short.json"
    synonym_filepath = "../data/dedup4_merge_2_3_dedup2_13aug21.json"

    load_synonym_lines(input_filepath=synonym_filepath, output_filepath=treetagger_syn_output_filename)
    load_homonym_lines(input_filepath=homonym_filepath, output_filepath=treetagger_homo_output_filename)

    synonyms = apply_treetagger(input_filepath=treetagger_syn_output_filename)
    homonyms = apply_treetagger_for_homonyms(input_filepath=treetagger_homo_output_filename)

    # generate inverse mapping: every word in line maps to the first word in the line
    # e.g. [A, B, C] => [B=>A, C=>A]
    normalized_synonym_json_data = generate_inverse_mapping(synonyms)

    print("Total Homonyms: ", len(homonyms), end="\n")
    print("Synonyms Before: ", len(normalized_synonym_json_data))
    normalized_synonym_json_data = filter_synonyms_by_homonyms(normalized_synonym_json_data, homonyms)
    print("Synonyms After: ", len(normalized_synonym_json_data))

    # Remove all synonyms that have only one mapping
    matches = Counter(normalized_synonym_json_data.values())
    copy_synonyms = normalized_synonym_json_data.copy()
    for word, word_sentinel in copy_synonyms.items():
        if matches[word_sentinel] == 1:
            normalized_synonym_json_data.pop(word)

    print("Synonyms After 2: ", len(normalized_synonym_json_data))
    with open('../data/inverse_mapping_dedup4_merge_2_3_dedup2.json', 'w') as file:
        json.dump(normalized_synonym_json_data, file)

    os.remove(treetagger_syn_output_filename)
    os.remove(treetagger_homo_output_filename)

    print("Translation done", end="\n")


if __name__ == "__main__":
    main()
