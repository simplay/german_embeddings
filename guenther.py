import json
import gensim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import openpyxl
import pickle
import seaborn as sns

#scritp produced 11.august to test freshly trained cbow model on whole corpus (including subtitles) and inverse mapping

cat_vecs = os.path.dirname('/mnt/Data/german_embeddings/guenther/')
#model_analysis = os.path.dirname('C:/Users/Magdalena/Documents/PhD/Projekt2/analysis/')

os.chdir(cat_vecs)

with open(r"vectors_dewac_cbow.txt", "r", encoding='cp1252') as file:
    cont_p = file.readlines()

dic = {}
for i in cont_p:
    lst = i.split('"')
    lst2 = lst[2].split(' ')
    num = lst2[1:]
    num_lst = num[-1].split('\n')
    num[-1] = num_lst[0]
    dic[lst[1]] = num

data = pd.read_excel('AWT_16_cats_cos1_cos2.xlsx')

w2 = list(data['w2'])
w1 = list(data['w1'])
vp = list(data['VP'])
id = list(data['ID'])

#replace Umlaute
lst_w1_1 = []
for i in w1:
    rep = i.replace('ä','ae')
    rep = rep.replace('ö','oe')
    rep = rep.replace('ü','ue')
    rep = rep.replace('Ü', 'Ue')
    rep = rep.replace('Ä', 'Ae')
    rep = rep.replace('Ö', 'Oe')
    rep = rep.replace('ß','ss')
    lst_w1_1.append(rep)

lst_w2 = []
for i in w2:
    rep = i.replace('ä','ae')
    rep = rep.replace('ö','oe')
    rep = rep.replace('ü','ue')
    rep = rep.replace('Ü', 'Ue')
    rep = rep.replace('Ä', 'Ae')
    rep = rep.replace('Ö', 'Oe')
    rep = rep.replace('ß','ss')
    lst_w2.append(rep)

lst_skip = []
lst_w2_corr = []
for idx,i in enumerate(lst_w2):
    i = i.rstrip()
    if ' ' in i:
        lst_skip.append(idx)
    else:
        lst_w2_corr.append(i)

lst_skip.sort(reverse=True)

for i in lst_skip:
    del id[i]
    del lst_w1_1[i]
    del vp[i]

#important: apply inverse mapping on target words and answers
lst_w1 = lst_w1_1.copy()
w1_dedup_2 = list(set(lst_w1))
vp_dedup = list(set(vp))

# get cosine similarity for word pairs
from numpy import dot
from numpy.linalg import norm

def cosine_similarity(list_1, list_2):
    cos_sim = dot(np.array(list_1,dtype=float), np.array(list_2,dtype=float)) / (norm(list_1) * norm(list_2))
    return cos_sim

df_new = pd.DataFrame({'ID': id,'VP':vp,'w1': lst_w1, 'w2': lst_w2_corr})

lst_fail = []
lst_dfs_cos = []
for i in w1_dedup_2:
    for j in vp_dedup:
        df_1 = df_new[df_new['w1'].str.contains(i)]
        df_2 = df_1[df_1['VP'].str.contains(j)]
        if len(df_2) != 0:
            lst_ans = list(df_2.w2)
            lst_freq = []
            for idx,k in enumerate(lst_ans):
                try:
                    v1 = dic[i.lower()]
                    v2 = dic[k.lower()]
                    lst_freq.append(cosine_similarity(v1, v2))
                except:
                    lst_freq.append('n.a.')
                    lst_fail.append({'w1': i, 'VP': j, 'w2': k})
            m_cos = np.mean([x for x in lst_freq if x != 'n.a.'])
            m_std = np.std([x for x in lst_freq if x != 'n.a.'])
            df_3 = pd.DataFrame({'ID': list(df_2.ID),'VP': [j]*(len(lst_freq)),'w1': [i]*(len(lst_freq)), 'w2': lst_ans, 'cos_dewac_2':lst_freq,'m_dewac_2':[m_cos]*(len(lst_freq)),'std_dewac_2':[m_std]*(len(lst_freq))})
            lst_dfs_cos.append(df_3)
res_cos2 = pd.concat(lst_dfs_cos)

data2= data.set_index("ID").join(res_cos2.set_index("ID")[['cos_dewac_2','m_dewac_2','std_dewac_2']])
data2.to_excel('AWT_16_dewac_cos2.xlsx',index=True)
###############################calculate different kind of cosine distance: get path length####################################

data2 = pd.read_excel('AWT_16_dewac_cos2.xlsx')

lst_fail = []
lst_dfs_cos = []
for i in w1_dedup_2:
    for j in vp_dedup:
        df_1 = df_new[df_new['w1'].str.contains(i)]
        df_2 = df_1[df_1['VP'].str.contains(j)]
        if len(df_2) != 0:
            lst_ans = list(df_2.w2)
            lst_freq = []
            for idx,k in enumerate(lst_ans):
                if idx ==0:
                    continue
                else:
                    try:
                        v1 = dic[lst_ans[idx-1].lower()]
                        v2 = dic[k.lower()]
                        lst_freq.append(cosine_similarity(v1, v2))
                    except:
                        lst_freq.append('n.a.')
                        lst_fail.append({'w1':i,'VP':j,'idx':k,'idx-1':lst_ans[idx-1]})
            m_cos = np.mean([x for x in lst_freq if x != 'n.a.'])
            m_std = np.std([x for x in lst_freq if x != 'n.a.'])
            df_3 = pd.DataFrame({'ID': list(df_2.ID),'VP': [j]*(len(lst_freq)+1),'w1': [i]*(len(lst_freq)+1), 'w2': lst_ans, 'cos_dewac':lst_freq+[np.nan],'m_dewac':[m_cos]*(len(lst_freq)+1),'std_dewac':[m_std]*(len(lst_freq)+1)})
            lst_dfs_cos.append(df_3)

res_cos1 = pd.concat(lst_dfs_cos)

data3= data2.set_index("ID").join(res_cos1.set_index("ID")[['cos_dewac','m_dewac','std_dewac']])

data3.to_excel('AWT_16_dewac.xlsx',index=True)