# German Embeddings

## Datasets

### Wikipedia

`https://dumps.wikimedia.org/dewiki/20210601/`

## Experiments

+ Datasets: (wiki, wiki+songs, wiki+songs+news)
+ Number of Vectors: 400
+ Korpus Filtering: (none, treetagger, treetagger + inverse-mapping)
+ Embedding method variations: (cbow, skipgram)


## Usage

Notice that intermediate files are stored in `out/`

### 1. Create Dataset

#### Wikipedia

```
wget https://dumps.wikimedia.org/dewiki/20210601/dewiki-20210601-pages-articles-multistream.xml.bz2
python3 -m wikiextractor.WikiExtractor -b 25M -o dewiki_01062021 dewiki-20210601-pages-articles-multistream.xml.bz2
find dewiki_01062021 -name '*' | xargs cat > dewiki_01062021.xml
sed -i 's/<[^>]*>//g' dewiki_01062021.xml
python3 preprocessing.py dewiki_01062021.xml corpus/dewiki_01062021.corpus -psub
python3 add_full_stop_character.py
```

#### Songs


`python3 process_songs.py`

#### News

Download the data sets from: http://www.statmt.org/wmt14/training-monolingual-news-crawl/

E.g., via `wget`:

```
wget http://www.statmt.org/wmt14/training-monolingual-news-crawl/news.2011.de.shuffled.gz
```

Extract the dataset via `gzip -d news.2011.de.shuffled.gz`. This will create a text file called `news.2011.de.shuffled`
Next, execute the script `process_news.py` located at the directory `news_processing`. 
Update the path to the extracted news text file (in this example `news.20111.de.shuffled`) and execute the script by running `python3 preprocess_news.py`

#### Movies

Optionally, pre-cleanup the movies via:
`sed -e "s/^M//" movie_subs.txt > movie_subs_clean.txt`

### 2. Filter Datasets

```
python3 generate_tree_tagger.py
python3 apply_tree_tagger.py
python3 remove_clutter_chars.py
```

#### Inverse Mapping (Optional Step)

To create an invese mapping, first execute the `main.py` located at `./synonymizer` package. Make sure that the synonym- and homonym files are correct. Apply the so-generated mapping to a corpus by executing

```
python3 apply_inverse_mapping.py
```

### 3. Generate Word Embedding

Assumption: there is a directory called `training_set_dewiki` that contains `*.corpus` some files.

```
# Create an embedding via cbow
python3 training.py training_set_dewiki model/dewiki_cbow_400.model -s 400 -w 5 -n 10 -m 50 -g 0

# Create an embedding via skip-gram
python3 training.py training_set_dewiki model/dewiki_skipgram_400.model -s 400 -w 5 -n 10 -m 50 -g 1
```

### Extract Statistics

`python3 extract_statistics.py`

## References

+ https://pypi.org/project/wikiextractor/
+ https://github.com/devmount/GermanWordEmbeddings
