UPDATE_ITERATION_COUNT = 2500


def write_to_output_file(filename: str, words: []) -> []:
    with open(filename, 'a') as file:
        content = "\n".join(words)
        file.write(content)
    return []


if __name__ == "__main__":
    final_lines = []
    base_name = "knorke"
    base_name = "dewiki_01062021"
    #base_name = "movie_subs"

    with open(f"corpus/{base_name}.corpus") as file:
        print("Reading file...")
        lines = file.read().split("\n")
        line_count = len(lines)

        for line_index, line in enumerate(lines, start=1):
            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")
            if len(line.split(" ")) < 4:
                continue

            # The whitespace between the line and the '.' is important for the tree-tagger stage.
            final_lines.append(f"{line} .")

    write_to_output_file(f"out/{base_name}.clean.corpus", final_lines)
