import gensim
from gensim.models.word2vec import Word2Vec
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

print("Loading model...")
model_path = os.join("model", "knorke_raw.model")
trained_model = gensim.models.KeyedVectors.load_word2vec_format(model_path, binary=True)
print("done")

word_vectors = trained_model.wv[trained_model.wv.vocab]

tsne = TSNE(n_components=2)
subset = word_vectors[0:2000]
word_vectors_tsne = tsne.fit_transform(subset)

plt.scatter(word_vectors_tsne[:, 0], word_vectors_tsne[:, 1])
plt.show()
