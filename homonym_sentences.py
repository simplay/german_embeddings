import os
import csv
import json
import collections
from collections import Counter

movie_mapped = "./out/movie_subs.final.inv_map_dedup4_merge_1_3_dedup2.corpus"
movie_clean = "./out/movie_subs.final.inv_map_dedup4_merge_1_3_dedup2.corpus"
news11_mapped = "./out/news_news2011.txt.mapped.corpus"
news11_clean = "./out/news_news2012.txt.mapped.corpus"
news12_mapped = "./out/news.2012.mapped.corpus"
news12_clean = "./out/news.2012.clean3.corpus"
dewiki_mapped = "./out/dewiki_01062021.mapped.corpus"
dewiki_clean = "./out/dewiki_01062021.clean.corpus"
songs_mapped = "./out/songs.final.inv_map_dedup4_merge_1_3_dedup2.corpus"
songs_clean = "./out/songs.final.inv_map_dedup4_merge_1_3_dedup2.corpus"

homonym_filepath = "./data/homonym_final_11aug21.csv"

UPDATE_ITERATION_COUNT = 250

def process_line(line: str) -> [str, bool]:
    splits = line.strip().split("\t")
    if not len(splits) == 3:
        return None, False

    raw_word, text_type, normalized_word = splits

    if raw_word == "@card@" or normalized_word == "@card@":
        return raw_word, True

    if len(normalized_word) < 2 and text_type[0] != "$":
        return raw_word, True

    if text_type[0] == "$":
        if raw_word == "!":
            return "!", False

        if raw_word == "." or raw_word == "?":
            return f"{raw_word}\n", True
        else:
            return raw_word, True


    elif text_type == "CARD":
        return raw_word, True

    elif text_type == "ADJA":
        # TODO: use raw_word here
        if normalized_word.__contains__("unknown"):
            return raw_word, True

        return normalized_word, True
    elif text_type == "ADV":
        if normalized_word.__contains__("unknown"):
            return raw_word, True

        # TODO: use raw_word here
        return normalized_word, True

    elif normalized_word == "@card@":
        # TODO: use raw_word here
        print(text_type)
        return raw_word, True


    # e.g.
    # "am" -> "an+die
    # "im" -> "in+die
    # "zum" -> "zu+di
    elif text_type == "APPRART":
        first_word, *_ = normalized_word.split("+")
        return first_word, True

    elif text_type == "NN":
        if normalized_word == "<unknown>":
            return raw_word, True

        # prevent false positives: flöten (used as an adjective) to "Flöte"
        if normalized_word[0] != raw_word[0]:
            return raw_word, True

        return normalized_word, True

    # foreign language words
    elif text_type == "FM":
        # TODO: can we remove this?
        return raw_word, True

    # locations, places
    elif text_type == "NE":
        return raw_word, True
    elif text_type == "WFIN":
        return normalized_word, True

    if normalized_word == "<unknown>":
        return raw_word, True

    normalized_word = normalized_word.replace("ß", "ss")

    if normalized_word == "<unknown>":
        return raw_word, True

    return normalized_word, True

def apply_treetagger_for_homonyms(input_filepath: str) -> list:
    cmd = f"sed -n '$=' {input_filepath}"
    line_count = int(os.popen(cmd).read())
    words = []

    lines = []
    with open(input_filepath) as file:
        for line_index, line in enumerate(file, start=1):

            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")

            word, should_append_word_to_list = process_line(line)

            if word.__contains__("unknown"):
                print(f"line {line} was mapped to unknown")

            if should_append_word_to_list:
                word = word.split("|")[0]
                word = word.strip().replace("ä", "ae")
                word = word.replace("ß", "ss")
                word = word.replace("ö", "oe")
                word = word.replace("ü", "ue")
                word = word.replace("Ä", "Ae")
                word = word.replace("Ö", "Oe")
                word = word.replace("Ü", "Ue")

                lines.append(word)

    return lines

def load_homonym_lines(input_filepath: str, output_filepath: str) -> list:
    with open(input_filepath) as file:
        reader = csv.reader(file, delimiter=';')
        items = []
        for row_index, row in enumerate(reader):
            if row_index == 0:
                continue
            else:
                items.append(row[0])
            #right_side = " ".join(row[1:]).strip()
            # INFO: this depends highly on the used dataset, currently, the quality of the dataset is rather poor.
            #if len(right_side.split(" ")) == 1:
                #items.append(row[0])
                #items.append(right_side)
            #else:
                #print(row)
    homonym_lines_filepath = f"homonym_lines.txt"
    with open(homonym_lines_filepath, 'w') as file:
        file.write("\n".join(items))

    cmd = f"./tree_tagger/cmd/tree-tagger-german {homonym_lines_filepath} > {output_filepath}"
    os.popen(cmd).read()
    os.remove(homonym_lines_filepath)

    return items

treetagger_homo_output_filename = "treetaggerd_hom.txt"

load_homonym_lines(input_filepath=homonym_filepath, output_filepath=treetagger_homo_output_filename)

homonyms = apply_treetagger_for_homonyms(input_filepath=treetagger_homo_output_filename)

lst_hom = list(set(homonyms))

lst_hom_correct = []
for i in homonyms:
    occ = homonyms.count(i)
    if occ < 2:
        continue
    elif i not in lst_hom_correct:
        lst_hom_correct.append(i)

# pure_lst = []
# with open(news12_mapped) as file:
#     for line_index, line in enumerate(file):
#         pure_lst.append(line)

#pure_lst2 = []
# with open(news12_mapped) as file:
#     for line_index, line in enumerate(file):
#         pure_lst2.append(line)
#
# pure_lst = []
# with open(news12_clean) as file:
#     for line_index, line in enumerate(file, start=1):
#         lines = line.split(".")
#         pure_lst = pure_lst+lines

hom_dic = {}
with open(news12_mapped) as file:
    for k in lst_hom_correct:
        lst_all = []
        for line_index, line in enumerate(file, start=1):
            wrd = f" {k} "
            if wrd in line:
                lst_all.append(line)
        hom_dic[k] = lst_all

# hom_dic = {}
# with open(news12_mapped) as file:
#     for line_index, line in enumerate(file, start=1):
#         for k in lst_hom_correct:
#             wrd = f" {k} "
#             if wrd in line:
#                 hom_dic[f"{k}{line_index}"] = pure_lst[line_index]
#
# od = collections.OrderedDict(sorted(hom_dic.items()))

with open('./news.2012_homs_original.json', 'w') as file:
    json.dump(hom_dic, file)

dic_short = {}
for k,v in hom_dic.items():
    if len(v) == 0:
        continue
    else:
        dic_short[k] = v

with open('./news.2012_Abdruck_short.json', 'w') as file:
    json.dump(dic_short, file)


count=0
for i in list(hom_dic.values()):
    if len(i) == 0:
        count = count+1