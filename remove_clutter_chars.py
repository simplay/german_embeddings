import re
from nltk.corpus import stopwords

UPDATE_ITERATION_COUNT = 2500


def normalize_word(word: str):
    word = word.replace('ä', 'ae')
    word = word.replace('ö', 'oe')
    word = word.replace('ü', 'ue')
    word = word.replace('Ä', 'Ae')
    word = word.replace('Ö', 'Oe')
    word = word.replace('Ü', 'Ue')
    word = word.replace('ß', 'ss')
    return word


def write_to_output_file(filename: str, words: []) -> []:
    with open(filename, 'a') as file:
        content = '\n'.join(words)
        file.write(content)
    return []


def load_misfits() -> list:
    misfits = stopwords.words('german')
    misfits = map(lambda word: normalize_word(word), misfits)
    return list(misfits)


def main(base_name: str):
    final_lines = []
    spaces = re.compile(r"\s+")
    numbers = re.compile(r"(\d+)((er){0,1})")
    too_short_acronyms = re.compile(r"[A-Z]{1,2}")
    number_word = re.compile(r"(\d+)([A-z]+)")

    input_filepath = f"out/{base_name}.mapped.corpus"
    #input_filepath = f"out/{base_name}.corpus"
    output_filepath = f"out/{base_name}.final.corpus"

    misfits = load_misfits()

    with open(input_filepath) as file:
        print("Reading file...")
        lines = file.read().split("\n")
        line_count = len(lines)

        for line_index, line in enumerate(lines, start=1):
            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")

            if len(line.split(" ")) < 4:
                continue

            line = line.strip().replace("ä", "ae")
            line = line.replace("ö", "oe")
            line = line.replace("ü", "ue")
            line = line.replace("Ä", "Ae")
            line = line.replace("Ö", "Oe")
            line = line.replace("Ü", "Ue")
            line = line.replace(".", "")
            line = line.replace("ß", "ss")
            line = line.replace("<unknown>", "")

            line = line.replace("-", " ")
            line = line.replace("−", " ")
            line = line.replace("–", " ")
            line = line.replace("‒", " ")

            line = line.replace("…", " ")
            line = line.replace(",", " ")
            line = line.replace("’", "")
            line = line.replace("\"", "")
            line = line.replace("\\", "")
            line = line.replace("„", "")
            line = line.replace("“", "")
            line = line.replace(")", " ")
            line = line.replace("(", " ")
            line = line.replace("]", " ")
            line = line.replace("[", " ")
            line = line.replace("/", " ")
            line = line.replace("@", " ")
            line = line.replace("%", "")
            line = line.replace("·", "")
            line = line.replace("+", " ")
            line = line.replace("'", " ")
            line = line.replace("´", " ")
            line = line.replace("http://", "")
            line = line.replace("www", "")
            line = line.replace("►", "")
            line = line.replace("€", "")
            line = line.replace(":", " ")
            line = line.replace("»", " ")
            line = line.replace("«", " ")
            line = line.replace("^", "")

            # remove undesired word patters
            line = " ".join(["" if too_short_acronyms.fullmatch(word) else word for word in line.split(" ")])
            line = " ".join(["" if numbers.fullmatch(word) else word for word in line.split(" ")])
            line = " ".join(["" if numbers.fullmatch(word) else word for word in line.split(" ")])

            line = " ".join([number_word.fullmatch(word)[2] if number_word.fullmatch(word) else word for word in
                             line.split(" ")])

            # remove misfits
            line = " ".join(["" if misfits.__contains__(word) else word for word in line.split(" ")])

            # ensure that there is only one whitespace separation between two words
            line = spaces.sub(" ", line)

            final_lines.append(line.strip())

    write_to_output_file(output_filepath, final_lines)


if __name__ == "__main__":
    base_name = "knorke"
    base_name = "songs"
    base_name = "dewiki_01062021"
    # base_name = "news_2011"
    # base_name = "news_2012"
    #base_name = "dewiki_01062021_treetaggerless"
    #base_name = "movie_subs"

    main(base_name)
