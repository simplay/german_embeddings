import gensim
from gensim.models import Word2Vec

print("Loading model...")
model = "model/knorke.final.model"
trained_model = gensim.models.KeyedVectors.load_word2vec_format(model, binary=True)
print("done")

word_vectors = trained_model.wv
print("Semantic Word Arithmetic:")

# Example
# Word A: Gold
# Word B: Heu
# Word C: Silber
# Stroh: 0.97853
while True:
    print("Provide the words A, B and C where D = A + B - C")
    word_a = input("Word A: ")
    word_b = input("Word B: ")
    word_c = input("Word C: ")
    result = word_vectors.most_similar_cosmul(positive=[word_a, word_b], negative=[word_c])
    most_similar_key, similarity = result[0]
    print(f"{most_similar_key}: {similarity:.5f}")
