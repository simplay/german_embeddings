import gensim
from gensim.models import Word2Vec
import os
import json
import time

print("Loading model...")
# trained_model = gensim.models.KeyedVectors.load_word2vec_format("german.model", binary=True)
base_name = "knorke.final"
base_name = "knorke.final.combined"
# base_name = "knorke_inv_mapping"
base_name = "dewiki_songs"
base_name = "invmap_dewiki_songs"
base_name = "dewiki_songs_news1112"
base_name = "dewiki_songs_news1112_400"
base_name = "dewiki_songs_news1112_skipgram"
base_name = "invmap_dewiki_songs_news1112_cbow"
base_name = "invmap_dewiki_songs_news1112_skipgram_400"
base_name = "dewiki_songs_skipgram_400"
base_name = "dewiki_songs_cbow_400"
base_name = "invmap_dewiki_songs_cbow_400"
base_name = "invmap_dewiki_songs_skipgram_400"
base_name = "dewiki_skipgram_400"
base_name = "dewiki_cbow_400"
base_name = "invmap_dewiki_cbow_400"
base_name = "invmap_dewiki_skipgram_400"
base_name = "dewiki_treetaggerless_skipgram_400"
base_name = "dewiki_treetaggerless_cbow_400"
base_name = "invmap_dewiki_songs_news1112_cbow"
base_name = "invmap_dewiki_songs_news1112_cbow"
base_name = "dewiki_songs_news1112_subs_skipgram_400"
base_name = "dewiki_songs_news1112_subs_cbow_400"
base_name = "inv_dewiki_songs_news1112_subs_cbow_400"
base_name = "inv_dewiki_songs_news1112_subs_skipgram_400"

input_filepath = f"model/{base_name}.model"

trained_model = gensim.models.KeyedVectors.load_word2vec_format(input_filepath, binary=True)
print("done")

most_similar_words = trained_model.most_similar('Heu')
trained_model.similarity("Heu", "Mann")
print(most_similar_words)

base_path = os.getcwd()
answer_path = os.path.join(base_path, "answers")

answer_file_paths = [file for file in os.listdir(answer_path) if os.path.isfile(os.path.join(answer_path, file))]

filter_tokens = ["\"", str(""), "-", "?", "/", "(", ")", ",", "-", ";"]
statistics = {}
for idx, answer_file_path in enumerate(answer_file_paths):
    input_file_path = os.path.join(answer_path, answer_file_path)

    with open(input_file_path, encoding="utf-8") as file:
        lines = json.load(file)
        lines = [line.strip() for line in lines]


    # with open(input_file_path, encoding="iso-8859-1") as file:
    #     lines = file.readlines()
    #     lines = [line.strip() for line in lines]

        filtered_lines = []
        for line in lines:
            print(line)
            if filter_tokens.__contains__(line):
                continue

            filtered_lines.append(line)

        target_word = answer_file_path.replace("_improved_upper.json", '').capitalize()

        best_matches = trained_model.most_similar(target_word)

        answer_similarities = {}
        for answer in filtered_lines:
            # TODO: filter this in preprocessing stage

            answer = answer.replace("ö", "oe")
            answer = answer.replace("ä", "ae")
            answer = answer.replace("ü", "ue")
            answer = answer.replace('ß', "ss")

            answer = answer.split("|")[0]
            answer = answer.split("/")[0]

            similarity = 0
            try:
                if answer == "eis":
                    print("eeew")
                similarity = float(trained_model.similarity(target_word, answer))
            except Exception as exception:
                print(exception)
                print(f"No similarity for answer {answer} and target word {target_word} found. Setting value to 0.")

            if similarity == 0:
                try:
                    answer = answer.capitalize()
                    similarity = float(trained_model.similarity(target_word, answer))
                except Exception as exception:
                    print(exception)
                    print(f"No similarity for answer {answer} and target word {target_word} found. Setting value to 0.")

            answer_similarities[answer] = similarity

        best_match_terms, best_match_similarities = list(zip(*best_matches))
        best_match_top10_features = [trained_model[term].tolist() for term in best_match_terms]

        best_match_top10_vectors = dict(zip(best_match_terms, best_match_top10_features))

        sorted_answer_similarities = dict(
            sorted(
                answer_similarities.items(),
                key=lambda item: -item[1]
            )
        )
        answer_terms = list(sorted_answer_similarities.keys())[0:20]
        answer_top20_features = [trained_model[term].tolist() for term in answer_terms]

        answers_top20_vectors = dict(zip(answer_terms, answer_top20_features))

        statistics[target_word] = {
            "answer_cosine_distances": answer_similarities,
            "best_matches_cosine_distances": best_matches,
            "best_match_top10_vectors": best_match_top10_vectors,
            "answer_top20_vectors": answers_top20_vectors
        }

current_milliseconds = round(time.time() * 1000)
with open(f"statistics_{current_milliseconds}.json", 'w') as file:
    json.dump(statistics, file)

print("foo")
