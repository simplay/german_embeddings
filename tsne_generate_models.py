import gensim
from gensim.models.word2vec import Word2Vec
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import os
import glob
import pickle
import json

script_dir = os.path.dirname('/mnt/Data/german_embeddings/')


os.chdir(os.path.join(script_dir, 'model/'))

xml_files = [os.path.join('model/', file) for file in glob.glob("*.model")]
print(xml_files)


for i in xml_files:
    os.chdir(script_dir)
    print("Loading model...")
    trained_model = gensim.models.KeyedVectors.load_word2vec_format(i, binary=True)
    print("done")

    model_dict = trained_model.wv.vocab

    word_vectors = trained_model.wv[model_dict]

    tsne = TSNE(n_components=2)

    word_vectors_tsne = tsne.fit_transform(word_vectors)

    wrds = model_dict.keys()
    vecs = list(word_vectors_tsne)

    c=[list(x) for x in vecs]

    res = dict(zip(wrds, c))

    nam = f'{i[6:-6]}.pkl'

    os.chdir(os.path.join(script_dir, 'tsne/'))

    a_file = open(nam, "wb")
    pickle.dump(res, a_file)
