import os
import glob
import re

def write_to_output_file(filename: str, articles: []) -> []:
    with open(filename, 'w') as file:
        file.write(articles[0])
    return []


def extract_news_content(filename, articles):
    base_path = "/mnt/Data/german_embeddings/"
    filepath = os.path.join(base_path, filename)
    song_file_pattern = f"{filepath}"
    for filepath in glob.glob(song_file_pattern):
        with open(filepath) as file:
            lines = file.read()
            lines = lines.replace("?", ".")
            lines = lines.replace("!", ".")
            lines = lines.replace("…", "")
            lines = lines.replace(",", "")
            lines = lines.replace("’", "")
            lines = lines.replace("ß", "ss")
            lines = lines.replace("-", " ")
            lines = lines.replace("…", " ")
            lines = lines.replace(",", " ")
            lines = lines.replace("’", "")
            lines = lines.replace("\"", "")
            lines = lines.replace("„", "")
            lines = lines.replace("»", "")
            articles.append(lines)

    return articles


if __name__ == "__main__":
    articles = []
    filename = "news.2012.de.shuffled"

    articles = extract_news_content(filename, articles)
    write_to_output_file(f"/mnt/Data/german_embeddings/out/news_{filename}.clean.corpus", articles)

    print("Removing tags")
    cmd = f"sed -i 's/<[^>]*>//g' /mnt/Data/german_embeddings/out/news_{filename}.clean.corpus"
    print(os.popen(cmd).read())
