import os
import json
import gensim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

base = os.path.dirname('/mnt/Data/german_embeddings/')
model = os.path.dirname('/mnt/Data/german_embeddings/model/')
inv = os.path.dirname('/mnt/Data/german_embeddings/data/')

os.chdir(base)

lst_ans = pd.read_excel('AWT_28sep21.xlsx')

df_tot = lst_ans[lst_ans['fluency'].isin([1])]
w2 = list(df_tot['answer short 1'])
w1 = list(df_tot['wordte'])
id = list(df_tot['ID'])
vp = list(df_tot['VP'])
vp_dedup = list(set(vp))
w1_dedup = list(set(w1))

num = []
for i in id:
    num.append(i.split('_')[-1])

####################load model, preprocess short answers######################################
os.chdir(inv)
with open('inverse_mapping_dedup4_merge_1_3_dedup2.json') as json_file:
    syns_all = json.load(json_file)

os.chdir(model)
print("Loading model...")
trained_model = gensim.models.KeyedVectors.load_word2vec_format("invmap_dedup4_merge1_3_dedup2_subs_skipgram_400.model", binary=True)
print("done")

os.chdir(base)

#replace Umlaute
lst_w1_1 = []
for i in w1:
    rep = i.replace('ä','ae') # i.replace(['ä','ö','ü','ß'],['ae','oe','ue','ss])
    rep = rep.replace('ö','oe')
    rep = rep.replace('ü','ue')
    rep = rep.replace('Ü', 'Ue')
    rep = rep.replace('Ä', 'Ae')
    rep = rep.replace('Ö', 'Oe')
    rep = rep.replace('ß','ss')
    lst_w1_1.append(rep)

lst_w2 = []
for i in w2:
    rep = i.replace('ä','ae')
    rep = rep.replace('ö','oe')
    rep = rep.replace('ü','ue')
    rep = rep.replace('Ü', 'Ue')
    rep = rep.replace('Ä', 'Ae')
    rep = rep.replace('Ö', 'Oe')
    rep = rep.replace('ß','ss')
    lst_w2.append(rep)

lst_skip = []
lst_w2_corr = []
for idx,i in enumerate(lst_w2):
    i = i.rstrip()
    if ' ' in i:
        lst_skip.append(idx)
    else:
        lst_w2_corr.append(i)

lst_skip.sort(reverse=True)

for i in lst_skip:
    del id[i]
    del lst_w1_1[i]
    del vp[i]
    del num[i]

#important: apply inverse mapping on target words and answers
lst_w1 = lst_w1_1.copy()
for idx,i in enumerate(lst_w1):
    try:
        lst_w1[idx] = syns_all[lst_w1[idx]]
    except:
        continue

for idx,i in enumerate(lst_w2_corr):
    try:
        lst_w2_corr[idx] = syns_all[lst_w2_corr[idx]]
    except:
        continue

w1_dedup_2 = list(set(lst_w1))

df_new = pd.DataFrame({'ID': id,'num':num,'vp':vp,'w1': lst_w1, 'w2': lst_w2_corr})

############calculate forward flow###############################

lst_dfs_cos = []
lst_fail1 = []
for i in w1_dedup_2:
    for j in vp_dedup:
        df_1 = df_new[df_new['w1'].str.contains(i)]
        df_2 = df_1[df_1['vp'].str.contains(j)]
        if len(df_2) != 0:
            lst_ans = [i]+list(df_2.w2)
            lst_freq = []
            lst_fail2 = []
            for px, p in enumerate(lst_ans):
                try:
                    v = trained_model[p]
                    lst_freq.append(p)
                except:
                    lst_fail2.append(px)
                    lst_fail1.append({'w1': i, 'VP': j, 'w2': p})

            for o in lst_fail2:
                lst_ans[o] = np.nan

            mat = np.zeros((len(lst_freq),len(lst_freq)))
            for ik,k in enumerate(lst_freq):
                for ia,a in enumerate(lst_freq):
                    mat[ik, ia] = trained_model.similarity(k, a)

            lst_val = []
            for w in range(np.shape(mat)[0]):
                if w != 0:
                    lst_val.append(np.mean(mat[w,:w]))
            ff = np.mean(lst_val)

            df_3 = pd.DataFrame(
                {'ID_1': list(df_2.ID),'num': list(df_2.num), 'VP_1': [j] * len(list(df_2.w2)), 'w1': [i] * len(list(df_2.w2)),
                 'w2_original':list(df_2.w2),'w2': lst_ans[1:], 'ff': [ff] * len(list(df_2.w2))})

            lst_dfs_cos.append(df_3)

res_cos = pd.concat(lst_dfs_cos)
res_cos.to_excel('ff_M1_29sep21.xlsx',index=False)

############calculate semantic distance between answers################################
lst_fail = []
lst_dfs_cos = []
for i in w1_dedup_2:
    for j in vp_dedup:
        df_1 = df_new[df_new['w1'].str.contains(i)]
        df_2 = df_1[df_1['vp'].str.contains(j)]
        if len(df_2) != 0:
            lst_ans = list(df_2.w2)
            lst_freq = []
            for idx,k in enumerate(lst_ans):
                if idx ==0:#to be improved: actually distance from target to first answer is missing!!
                    continue
                else:
                    try:
                        lst_freq.append(trained_model.similarity(lst_ans[idx-1],k))
                    except:
                        lst_freq.append('n.a.')
                        lst_fail.append({'w1':i,'VP':j,'idx':k,'idx-1':lst_ans[idx-1]})
            m_cos = np.mean([x for x in lst_freq if x != 'n.a.'])
            m_std = np.std([x for x in lst_freq if x != 'n.a.'])
            df_3 = pd.DataFrame({'ID_1': list(df_2.ID),'VP_1': [j]*(len(lst_freq)+1),'w1_1': [i]*(len(lst_freq)+1), 'w2_2': lst_ans, 'cos_M1':lst_freq+[np.nan],'m_cos_M1':[m_cos]*(len(lst_freq)+1),'m_std_M1':[m_std]*(len(lst_freq)+1)})
            lst_dfs_cos.append(df_3)

res_cos = pd.concat(lst_dfs_cos)

df_ff = pd.read_excel('ff_M1_29sep21.xlsx')

data= df_ff.set_index("ID_1").join(res_cos.set_index("ID_1")[['cos_M1','m_cos_M1','m_std_M1']])
data.to_excel('ff_mean_std_cos_M1_29sep21.xlsx',index=True)

##########################get different cos-values: difference between target word and answer-word#################
lst_fail = []
lst_dfs_cos = []
for i in w1_dedup_2:
    for j in vp_dedup:
        df_1 = df_new[df_new['w1'].str.contains(i)]
        df_2 = df_1[df_1['vp'].str.contains(j)]
        if len(df_2) != 0:
            lst_ans = list(df_2.w2)
            lst_freq = []
            for idx,k in enumerate(lst_ans):
                try:
                    lst_freq.append(trained_model.similarity(i,k))
                except:
                    lst_freq.append('n.a.')
                    lst_fail.append({'w1': i, 'VP': j, 'w2': k})
            m_cos = np.mean([x for x in lst_freq if x != 'n.a.'])
            m_std = np.std([x for x in lst_freq if x != 'n.a.'])
            df_3 = pd.DataFrame({'ID': list(df_2.ID),'VP': [j]*(len(lst_freq)),'w1': [i]*(len(lst_freq)), 'w2': lst_ans, 'cos2_M1':lst_freq,'m_cos2_M1':[m_cos]*(len(lst_freq)),'m_std2_M1':[m_std]*(len(lst_freq))})
            lst_dfs_cos.append(df_3)
res_cos2 = pd.concat(lst_dfs_cos)

df_ff2 = pd.read_excel('ff_mean_std_cos_M1_29sep21.xlsx')

data= df_ff2.set_index("ID_1").join(res_cos2.set_index("ID")[['cos2_M1','m_cos2_M1','m_std2_M1']])
data.to_excel('ff_mean1_2_std1_2_cos_M1_29sep21.xlsx',index=True)

####################get number of different categories per participant per target word###########################

lst_dfs_cats = []
for i in w1_dedup:
    for j in vp_dedup:
        df_1 = df_tot[df_tot['wordte'].str.contains(i)]
        df_2 = df_1[df_1['VP'].str.contains(j)]
        if len(df_2) != 0:
            num_cats = len(list(set(df_2['category 1'])))
            df_3 = pd.DataFrame({'ID': list(df_2.ID),'VP': [j]*(len(list(df_2.answer))),'w1': [i]*(len(list(df_2.answer))), 'w2': list(df_2['answer short 1']), 'cats': list(df_2['category 1']), 'num_cat':[num_cats]*(len(list(df_2.answer)))})
            lst_dfs_cats.append(df_3)

res_cats = pd.concat(lst_dfs_cats)

df_ff3 = pd.read_excel('ff_mean1_2_std1_2_cos_M1_29sep21.xlsx')

data= df_ff3.set_index("Unnamed: 0").join(res_cats.set_index("ID")[['cats','num_cat']])
data.to_excel('ff_mean1_2_std1_2_cos_cats_M1_30sep21.xlsx',index=True)

#######################################calculate frequencies#########################################################################################################
data = pd.read_excel('ff_mean1_2_std1_2_cos_cats_M1_30sep21.xlsx')
w2 = list(data.w2_original)
w1 = list(data.w1)
vp = list(data.VP)
id = list(data.ID)

df_schaf = data[data['w1'].str.contains('Hammel')]

df_adler = data[data['w1'].str.contains('Adler')]

df_teppich = data[data['w1'].str.contains('Teppich')]

df_tisch = data[data['w1'].str.contains('Festtafel')]

df_haus = data[data['w1'].str.contains('Haus')]

df_schere = data[data['w1'].str.contains('Schere')]

lst_dfs = [df_schaf,df_adler,df_teppich,df_tisch,df_haus,df_schere]

lst_dfs_new = []
for df_all in lst_dfs:
    ans = df_all.w2
    set_ = list(set(ans))
    all_ans = len(ans)

    lst_sum = {}
    for i in set_:
        count = 0
        for j in list(ans):
            if i == j:
                count = count + 1
        lst_sum[i] = count

    dic_end = {}
    for i, k in lst_sum.items():
        val = k / all_ans
        dic_end[i] = val

    lst_freqs = []
    for i in ans:
        lst_freqs.append(dic_end[i])

    words_all = pd.DataFrame({'ID': df_all.ID, 'VP': df_all.VP, 'w1': df_all.w1, 'w2': df_all.w2, 'frequency': lst_freqs})
    lst_dfs_new.append(words_all)

result = pd.concat(lst_dfs_new)


data_all = data.set_index("ID").join(result.set_index("ID")[['frequency']])

data_all.to_excel('ff_mean1_2_std1_2_cos_cats_orig_M1_30sep21.xlsx',index=True)