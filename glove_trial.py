from flair.embeddings import WordEmbeddings
from flair.data import Sentence
import os
import json
import gensim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

base = os.path.dirname('/mnt/Data/german_embeddings/')
os.chdir(base)

lst_ans = pd.read_excel('300_humrat_dewac_M1.xlsx')

lst_wrds = list(lst_ans['Wort1'])
lst_trgts = list(lst_ans['Wort2'])
id = list(lst_ans['ID'])

#replace Umlaute
lst_new = []
for i in lst_wrds:
    i = i.rstrip()
    rep = i.replace('ß','ss')
    lst_new.append(rep)

lst_new_trs = []
for i in lst_trgts:
    i = i.rstrip()
    rep = i.replace('ß','ss')
    lst_new_trs.append(rep)

from numpy import dot
from numpy.linalg import norm

def cosine_similarity(list_1, list_2):
    cos_sim = dot(np.array(list_1, dtype=float), np.array(list_2, dtype=float)) / (norm(list_1) * norm(list_2))
    return cos_sim

glove_embedding = WordEmbeddings('glove')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_glove':lst_freq})

data2= lst_ans.set_index("ID").join(df_new.set_index("ID")['cos_glove'])
data2.to_excel('300_humrat_dewac_M1_glove.xlsx',index=True)

#########################################trial_fasttext_german--crawl#########################################################################
lst_a1 = pd.read_excel('300_humrat_dewac_M1_glove.xlsx')

glove_embedding = WordEmbeddings('de-crawl')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new1 = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_FT_crawl':lst_freq})

data2_1 = lst_a1.set_index("ID").join(df_new1.set_index("ID")['cos_FT_crawl'])
data2_1.to_excel('300_humrat_dewac_M1_glove_FTcrawl.xlsx',index=True)

#########################################trial_fasttext_german--wiki#########################################################################

lst_a2 = pd.read_excel('300_humrat_dewac_M1_glove_FTcrawl.xlsx')

glove_embedding = WordEmbeddings('de')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new2 = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_FT_wiki':lst_freq})

data2_2 = lst_a2.set_index("ID").join(df_new2.set_index("ID")['cos_FT_wiki'])
data2_2.to_excel('300_humrat_dewac_M1_glove_FTcrawl_FTwiki.xlsx',index=True)

#######################################################################################goodenough#######################################################
from flair.embeddings import WordEmbeddings
from flair.data import Sentence
import os
import json
import gensim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

base = os.path.dirname('/mnt/Data/german_embeddings/')
os.chdir(base)

lst_ans = pd.read_excel('goodenough_8.xlsx')

lst_wrds = list(lst_ans['V1'])
lst_trgts = list(lst_ans['V2'])
id = list(lst_ans['ID'])

#replace Umlaute
lst_new = []
for i in lst_wrds:
    i = i.rstrip()
    rep = i.replace('ß','ss')
    lst_new.append(rep)

lst_new_trs = []
for i in lst_trgts:
    i = i.rstrip()
    rep = i.replace('ß','ss')
    lst_new_trs.append(rep)

from numpy import dot
from numpy.linalg import norm

def cosine_similarity(list_1, list_2):
    cos_sim = dot(np.array(list_1, dtype=float), np.array(list_2, dtype=float)) / (norm(list_1) * norm(list_2))
    return cos_sim

glove_embedding = WordEmbeddings('glove')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_glove':lst_freq})

data2= lst_ans.set_index("ID").join(df_new.set_index("ID")['cos_glove'])
data2.to_excel('goodenough_glove_dewac_M1.xlsx',index=True)

#########################################trial_fasttext_german--crawl#########################################################################
lst_a1 = pd.read_excel('goodenough_glove_dewac_M1.xlsx')

glove_embedding = WordEmbeddings('de-crawl')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new1 = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_FT_crawl':lst_freq})

data2_1 = lst_a1.set_index("ID").join(df_new1.set_index("ID")['cos_FT_crawl'])
data2_1.to_excel('goodenough_glove_FT1_dewac_M1.xlsx',index=True)

#########################################trial_fasttext_german--wiki#########################################################################

lst_a2 = pd.read_excel('goodenough_glove_FT1_dewac_M1.xlsx')

glove_embedding = WordEmbeddings('de')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new2 = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_FT_wiki':lst_freq})

data2_2 = lst_a2.set_index("ID").join(df_new2.set_index("ID")['cos_FT_wiki'])
data2_2.to_excel('goodenough_glove_FT1_FT2_dewac_M1.xlsx',index=True)



#######################################################################yang&powers##########################################################################################################
from flair.embeddings import WordEmbeddings
from flair.data import Sentence
import os
import json
import gensim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

base = os.path.dirname('/mnt/Data/german_embeddings/')
os.chdir(base)

lst_ans = pd.read_excel('yang_powers_dewac_9.xlsx')

lst_wrds = list(lst_ans['w1'])
lst_trgts = list(lst_ans['w2'])
id = list(lst_ans['d'])

#replace Umlaute
lst_new = []
for i in lst_wrds:
    i = i.rstrip()
    rep = i.replace('ß','ss')
    lst_new.append(rep)

lst_new_trs = []
for i in lst_trgts:
    i = i.rstrip()
    rep = i.replace('ß','ss')
    lst_new_trs.append(rep)

from numpy import dot
from numpy.linalg import norm

def cosine_similarity(list_1, list_2):
    cos_sim = dot(np.array(list_1, dtype=float), np.array(list_2, dtype=float)) / (norm(list_1) * norm(list_2))
    return cos_sim

glove_embedding = WordEmbeddings('glove')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_glove':lst_freq})

data2= lst_ans.set_index("d").join(df_new.set_index("ID")['cos_glove'])
data2.to_excel('yang&powers_glove_dewac_M1.xlsx',index=True)

#########################################trial_fasttext_german--crawl#########################################################################
lst_a1 = pd.read_excel('yang&powers_glove_dewac_M1.xlsx')

glove_embedding = WordEmbeddings('de-crawl')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})

df_new1 = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_FT_crawl':lst_freq})

data2_1 = lst_a1.set_index("d").join(df_new1.set_index("ID")['cos_FT_crawl'])
data2_1.to_excel('yang&powers_glove_FT1_dewac_M1.xlsx',index=True)

#########################################trial_fasttext_german--wiki#########################################################################

lst_a2 = pd.read_excel('yang&powers_glove_FT1_dewac_M1.xlsx')

glove_embedding = WordEmbeddings('de')

lst_freq = []
lst_fail = []
for idx, k in enumerate(lst_new):
    sentence = Sentence(f'{k} {lst_new_trs[idx]}')
    # embed a sentence using glove.
    glove_embedding.embed(sentence)
    vecs = []
    for token in sentence:
        vecs.append(token.embedding)
    try:
        v1 = vecs[0].cpu()
        v2 = vecs[1].cpu()
        lst_freq.append(cosine_similarity(list(v1.numpy()), list(v2.numpy())))
    except:
        lst_freq.append('n.a.')
        lst_fail.append({'w1': lst_new_trs[idx], 'w2': k})


df_new2 = pd.DataFrame({'ID': id, 'w1': lst_new_trs, 'w2': lst_new, 'cos_FT_wiki':lst_freq})

data2_2 = lst_a2.set_index("d").join(df_new2.set_index("ID")['cos_FT_wiki'])
data2_2.to_excel('yang&powers_glove_FT1_FT2_dewac_M1.xlsx',index=True)






lst_fail = []
lst_dfs_cos = []
for i in w1_dedup_2:
    for j in vp_dedup:
        df_1 = df_new[df_new['w1'].str.contains(i)]
        df_2 = df_1[df_1['VP'].str.contains(j)]
        if len(df_2) != 0:
            lst_ans = list(df_2.w2)
            lst_freq = []
            for idx, k in enumerate(lst_ans):
                try:
                    v1 = dic[i.lower()]
                    v2 = dic[k.lower()]
                    lst_freq.append(cosine_similarity(v1, v2))
                except:
                    lst_freq.append('n.a.')
                    lst_fail.append({'w1': i, 'VP': j, 'w2': k})
            m_cos = np.mean([x for x in lst_freq if x != 'n.a.'])
            m_std = np.std([x for x in lst_freq if x != 'n.a.'])
            df_3 = pd.DataFrame(
                {'ID': list(df_2.ID), 'VP': [j] * (len(lst_freq)), 'w1': [i] * (len(lst_freq)), 'w2': lst_ans,
                 'cos_dewac_2': lst_freq, 'm_dewac_2': [m_cos] * (len(lst_freq)),
                 'std_dewac_2': [m_std] * (len(lst_freq))})
            lst_dfs_cos.append(df_3)
res_cos2 = pd.concat(lst_dfs_cos)


# init embedding
glove_embedding = WordEmbeddings('glove')


# create sentence.
sentence = Sentence('König')

# embed a sentence using glove.
glove_embedding.embed(sentence)

# now check out the embedded tokens.
for token in sentence:
    print(token)
    print(token.embedding)














d = {'Rank': id, 'Wort1': lst_new_trs, 'Wort2': lst_new, 'M2': lst_freq}
df = pd.DataFrame(d)



