import os
import csv
import json
import collections
import re

news12_clean = "./out/news.2011.clean.corpus"


pure_lst2 = []
with open(news12_clean) as file:
    for line_index, line in enumerate(file):
        pure_lst2.append(line)

lst_pure_new = []
for i in pure_lst2:
    if len(i) > 3:
        if i[-2] != '.':
            i = i[:-1]+'.'+i[-1:]
            lst_pure_new.append(i)
        else:
            lst_pure_new.append(i)
    else:
        lst_pure_new.append((i))

lst_pure_new2 = []
for i in  lst_pure_new:
    if len(i) > 3:
        if len(i.split('.'))>2:
            lst = i.split('.')
            long_str = ''
            for idx,j in enumerate(lst):
                if len(j) > 3:
                    if j[-2] != ' ' and j[-3] != ' ':
                        long_str = f"{long_str} {j}.\n"
                    else:
                        long_str = f"{long_str} {j} "
                else:
                    if j == "\n":
                        continue
                    else:
                        print(j)
                        print(i)
                        long_str = f"{long_str} {j}.\n"
            lst_pure_new2.append(long_str)
        else:
            lst_pure_new2.append(i)
    else:
        lst_pure_new2.append(i)


filename = 'news.2011'

with open(f"/mnt/Data/german_embeddings/out/{filename}.clean3.corpus", 'w') as file:
    content = " ".join(lst_pure_new2)
    file.write(content)

