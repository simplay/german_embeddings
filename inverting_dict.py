import json
import os



def invert_dict(d):
    inverse = dict()
    for key in d:
        # Go through the list that is saved in the dict:
        for item in d[key]:
            # Check if in the inverted dict the key exists
            if item not in inverse:
                # If not create a new list
                inverse[item] = [key]
            else:
                inverse[item].append(key)
    return


def main():
    inverse_mapping_filepath = os.path.join("data", "short_twice_1_3_dedupifless2_05aug21.json")
    with open(inverse_mapping_filepath) as file:
        inverse_mapping = json.load(file)

    with open('data/test.json', 'w') as file:
        json.dump(invert_dict(inverse_mapping), file)



if __name__ == "__main__":
    main()