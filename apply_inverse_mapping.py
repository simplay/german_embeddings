import json
import os

UPDATE_ITERATION_COUNT = 250


def process_line(line: str, inverse_mapping: dict):
    transformation = []
    for word in line.strip().split(" "):
        # if word.__contains__("Luft"):
        #     print("eeeew")
        if inverse_mapping.__contains__(word):
            transformation.append(inverse_mapping[word])
        else:
            transformation.append(word)

    sentence = " ".join(transformation)
    return f"{sentence}\n"


def write_to_output_file(filename: str, words: []) -> []:
    with open(filename, 'a') as file:
        content = "\n".join(words)
        file.write(content)
    return []


def main():
    inverse_mapping_filepath = os.path.join("data", "inverse_mapping_dedup4_merge_2_3_dedup2.json")
    with open(inverse_mapping_filepath) as file:
        inverse_mapping = json.load(file)

    base_path = "out"

    base_name = "knorke"
    base_name = "dewiki_01062021"
    base_name = "songs"
    base_name = "news_2011"
    base_name = "news_2012"
    base_name = "movie_subs"

    korpus_filepath = os.path.join(base_path, f"{base_name}.final.corpus")

    output_filepath = f"out/{base_name}.final.inv_map_dedup4_merge_2_3_dedup2.corpus"

    cmd = f"sed -n '$=' {korpus_filepath}"
    line_count = int(os.popen(cmd).read())

    final_lines = []
    with open(korpus_filepath) as file:
        for line_index, line in enumerate(file, start=1):

            if line_index % UPDATE_ITERATION_COUNT == 0:
                progress = "{:.3f}".format(100 * (line_index / (line_count - 1)))
                print(f"\rProcessed {line_index} / {line_count} lines [{progress}%]", end="")

            line = process_line(line, inverse_mapping)

            final_lines.append(f"{line.strip()}")

    write_to_output_file(filename=output_filepath, words=final_lines)

    print("running main")


if __name__ == "__main__":
    main()
